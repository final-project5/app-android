package com.example.smrt;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class FeedbackActivity extends AppCompatActivity {
    AutoCompleteTextView streetEditText;
    EditText feedbackEditText;
    ImageView backBtn;
    Button submitBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        streetEditText = findViewById(R.id.feedBackStreetEditText);
        streetEditText.setAdapter(MainActivity.adapterStreetName);

        feedbackEditText = findViewById(R.id.textInputEditText);

        backBtn = findViewById(R.id.popupBackBtn);
        backBtn.setOnClickListener(view -> {finish();});

        submitBtn = findViewById(R.id.submitBtnFeedback);
        submitBtn.setOnClickListener(view -> {
            streetEditText.setText("");
            streetEditText.clearFocus();
            feedbackEditText.setText("");
            feedbackEditText.clearFocus();
        });
    }
}
