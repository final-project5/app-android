package com.example.smrt;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.navigation.NavigationView;

import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.example.smrt.api.CamsLoader;
import com.example.smrt.api.Router;
import com.example.smrt.model.Camera;
import com.example.smrt.model.MapPoint;
import com.example.smrt.utils.PopUp;
import com.example.smrt.utils.Routing;
import com.opencsv.CSVReader;

import org.json.JSONArray;
import org.json.JSONException;
import org.osmdroid.config.Configuration;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.BoundingBox;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Polyline;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider;
import org.osmdroid.views.overlay.infowindow.BasicInfoWindow;
import org.osmdroid.views.overlay.infowindow.InfoWindow;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.IMyLocationConsumer;
import org.osmdroid.views.overlay.mylocation.IMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity implements Routing, IMyLocationConsumer, PopUp {

  public static GeoPoint CENTER = new GeoPoint(16.054456, 108.222966);
  public static final int DEFAULT_ZOOM = 15;
  public static final int NAV_ZOOM = 19;
  public static boolean followUser = false;
  public static SharedPreferences prefs;

  public static HashMap<String,double[]> streetAndPos = new HashMap<>();
  public static ArrayAdapter adapterStreetName;
  public static IMyLocationProvider locProvider;

  private MyLocationNewOverlay myCurrentPos,myLocationNav;
  private MapView mapView;
  private TextView startTextView;
  private TextView endTextView;
  private TextView distanceTextView;
  private AutoCompleteTextView searchBox;
  private ConstraintLayout mainLayout;
  private ConstraintLayout routingInfoLayout;
  private ConstraintLayout choosePointLayout;
  private ConstraintLayout navLayout;
  private String activeInput;
  private Polyline rLine;
  private JSONArray rArray;
  private Marker startMarker;
  private Marker endMarker;
  private MapPoint startLoc;
  private MapPoint endLoc;
  private ImageView myLocationBtn;
  private ImageView directionBtn;
  private Button backRIBtn;
  private Marker searchMarker;
  private View startNavBut;
  private Button exitNavBtn;
  private CompassOverlay myCompass;
  private TextView nextDirTextView;
  private ImageView nextDirIcon;
  private TextView curDirTextView;
  private ImageView curDirIcon;

  @SuppressLint("ClickableViewAccessibility")
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    requestPermissions();

    Configuration.getInstance().setUserAgentValue(BuildConfig.APPLICATION_ID);
    prefs = this.getSharedPreferences("com.example.app", Context.MODE_PRIVATE);
    //Set saved id
    CamsLoader.updateIP(prefs.getString(CamsLoader.IP_KEY,CamsLoader.IP_ADDRESS));

    //Ignore Internet Security
    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    StrictMode.setThreadPolicy(policy);

    //Load street data from file
    loadStreetAndPos();

    Context ctx = getApplicationContext();
    Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
    setContentView(R.layout.activity_main_with_navbar);

    //SUB LAYOUT
    mainLayout = findViewById(R.id.mainLayout);
    routingInfoLayout = findViewById(R.id.routingInfoLayout);
    choosePointLayout = findViewById(R.id.choosePointLayout);
    navLayout = findViewById(R.id.navMapLayout);

    startTextView = findViewById(R.id.startTextView1);
    endTextView = findViewById(R.id.endTextView2);
    distanceTextView = findViewById(R.id.distanceTextView);
    myLocationBtn = findViewById(R.id.myLocaltionBtn);
    directionBtn = findViewById(R.id.directionBtn);
    backRIBtn = findViewById(R.id.backRIBtn);
    searchBox = findViewById(R.id.searchBox);
    startNavBut = findViewById(R.id.startNavRIBtn);
    exitNavBtn = findViewById(R.id.exitNavBtn);
    nextDirTextView = findViewById(R.id.nextDirection);
    nextDirIcon = findViewById(R.id.nextDirIcon);
    curDirTextView = findViewById(R.id.curDirection);
    curDirIcon = findViewById(R.id.curDirIcon);

    searchBox.setAdapter(adapterStreetName);

    //-----------SET_UP_MAP-----------
    mapView = findViewById(R.id.mapview);
    mapView.setClickable(true);
    //Ability to zoom with 2 fingers
    mapView.setBuiltInZoomControls(false);
    mapView.setMultiTouchControls(true);

    mapView.setUseDataConnection(true);
    mapView.setTilesScaledToDpi(true);
    mapView.setTileSource(TileSourceFactory.MAPNIK);

    // zoom to CENTER
    this.mapView.getController().setZoom(DEFAULT_ZOOM);
    this.mapView.getController().setCenter(CENTER);

    MapEventsReceiver mReceive = new MapEventsReceiver() {
      @Override
      public boolean singleTapConfirmedHelper(GeoPoint p) {
        InfoWindow.closeAllInfoWindowsOn(mapView);
        return false;
      }
      @Override
      public boolean longPressHelper(GeoPoint p) {
        return false;
      }
    };

    MapEventsOverlay OverlayEvents = new MapEventsOverlay(getBaseContext(), mReceive);
    mapView.getOverlays().add(OverlayEvents);

    //-----------User Location-----------
//    userStandBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_map_user_pos);
    locProvider = new GpsMyLocationProvider(this);
    locProvider.startLocationProvider(this);

    myCompass = new CompassOverlay(this.getApplicationContext(),
            new InternalCompassOrientationProvider(this.getApplicationContext()),
            this.mapView);
    myCompass.enableCompass();

    myCurrentPos = new MyLocationNewOverlay(mapView);
    myCurrentPos.enableMyLocation();
    Bitmap userStandPoint = BitmapFactory.decodeResource(getResources(), R.drawable.ic_map_user_pos);
    myCurrentPos.setDirectionArrow(userStandPoint,userStandPoint);
    mapView.getOverlays().add(myCurrentPos);

    myLocationNav = new MyLocationNewOverlay(mapView);
    myLocationNav.enableMyLocation();
    Bitmap userPoint = BitmapFactory.decodeResource(getResources(), R.drawable.ic_map_user_navi);
    myLocationNav.setDirectionArrow(userPoint,userPoint);
    //-----------User Location-----------

    //Load cameras to map
    new CamsLoader(this,mapView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    mapView.setOnTouchListener((view, motionEvent) -> {
      if(motionEvent.getAction()==MotionEvent.ACTION_DOWN) {
        if (searchBox.isFocused()) searchBox.clearFocus();
        ((InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(), 0);
      }
      return false;
    });
    //-----------/SET_UP_MAP-----------

    startTextView.setOnClickListener(view -> openRoutInpAct("start"));
    endTextView.setOnClickListener(view -> openRoutInpAct("end"));
    myLocationBtn.setOnClickListener(view -> {
      Location loc = locProvider.getLastKnownLocation();
//      LocationManager mLocationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
//      List<String> providers = mLocationManager.getProviders(true);
//      Location loc = null;
//      for (String provider : providers) {
//        Location l = mLocationManager.getLastKnownLocation(provider);
//        if (l == null) {
//          continue;
//        }
//        if (loc == null || l.getAccuracy() < loc.getAccuracy()) {
//          loc = l;
//        }
//      }
      if(loc!=null) this.mapView.getController().animateTo(new GeoPoint(loc.getLatitude(),loc.getLongitude()));
      this.mapView.getController().setZoom(DEFAULT_ZOOM);
    });

    //Go to point on enter search box
    searchBox.setOnEditorActionListener((v, actionId, event) -> {
      boolean handled = false;
      if (actionId == EditorInfo.IME_ACTION_SEARCH) {
        pointToPosition(searchBox.getText().toString());
      }
      return handled;
    });

    Button okButton = findViewById(R.id.okButton);
    okButton.setOnClickListener(view -> {
      GeoPoint selPoint = (GeoPoint) mapView.getMapCenter();
      String label = String.format("%.4f,%.4f",selPoint.getLatitude(),selPoint.getLongitude());
      if(activeInput.equals("start")) {
        startLoc.setLocation(selPoint.getLatitude(),selPoint.getLongitude());
        startLoc.setLabel(label);
        startLoc.setMode(3);
      }else{
        endLoc.setLocation(selPoint.getLatitude(),selPoint.getLongitude());
        endLoc.setLabel(label);
        endLoc.setMode(3);
      }
//      MapPoint mp = new MapPoint(3);
//      selPoint.getLatitude()+", "+selPoint.getLongitude(),selPoint.getLatitude(),selPoint.getLongitude()
//      if(activeInput.equals("start")) startTextView.setText(mp.getLabel());
//      else endTextView.setText(mp.getLabel());
      openRoutInpAct(activeInput);
    });

//    -----------SET_UP_Navigation_Bar-----------
    final DrawerLayout drawer = findViewById(R.id.drawer_layout);
    NavigationView navigationView = findViewById(R.id.nav_view);
    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    drawer.addDrawerListener(toggle);
    toggle.syncState();
    navigationView.setNavigationItemSelectedListener(new NavBarListener(this));

    ImageButton imageButton = findViewById(R.id.navBtn);
    imageButton.setOnClickListener(view -> drawer.openDrawer(GravityCompat.START));
    //-----------/SET_UP_Navigation_Bar-----------

    //Process on AdapterItem clicked
    searchBox.setOnItemClickListener((adapterView, v, i, l) -> {
      pointToPosition(adapterView.getItemAtPosition(i).toString());
    });

    //-----------Navigation mode-----------
    startNavBut.setOnClickListener(view -> {
      if(this.rLine==null) return;
      routingInfoLayout.setVisibility(View.INVISIBLE);
      navLayout.setVisibility(View.VISIBLE);
      startNavigation();
    });
    exitNavBtn.setOnClickListener(view -> {
      navLayout.setVisibility(View.INVISIBLE);
      mainLayout.setVisibility(View.VISIBLE);
      endNavigation();
    });
    directionBtn.setOnClickListener(view -> {
      mainLayout.setVisibility(View.INVISIBLE);
      routingInfoLayout.setVisibility(View.VISIBLE);
      distanceTextView.setVisibility(View.INVISIBLE);
      startNavBut.setVisibility(View.INVISIBLE);
//      TranslateAnimation animate = new TranslateAnimation(
//              routingInfoLayout.getWidth(),
//              0,
//              0,
//              0);
//      animate.setDuration(300);
//      animate.setFillAfter(true);
//      routingInfoLayout.startAnimation(animate);
    });
    backRIBtn.setOnClickListener(view -> {
      mainLayout.setVisibility(View.VISIBLE);
      routingInfoLayout.setVisibility(View.INVISIBLE);
      removeRoutingLine();
    });
    startLoc = new MapPoint(1);
    endLoc = new MapPoint(2);
    endLoc.setLocation(16.041569, 108.217488);
    endLoc.setLabel("");
//    endLoc.setLabel("Quang Dũng");
    //-----------/Navigation mode-----------
  }

  private void pointToPosition(String input) {
    try {
      searchBox.clearFocus();
      ((InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
      double[] coord = streetAndPos.get(input);
      mapView.getController().animateTo(new GeoPoint(coord[0], coord[1]));
      if (searchMarker != null) mapView.getOverlayManager().remove(searchMarker);
      searchMarker = addMarker(coord[0], coord[1],R.drawable.ic_map_marker);
    }catch (Exception e) {e.printStackTrace();}
  }

  //Load streets's name and their coordinates
  private void loadStreetAndPos() {
    read2streetAndPos("streets_data.csv");
    read2streetAndPos("places_data.csv");
    adapterStreetName = new ArrayAdapter(this,android.R.layout.simple_list_item_1, MainActivity.streetAndPos.keySet().toArray());
  }

  public void read2streetAndPos(String fileName){
    try(CSVReader reader = new CSVReader(new InputStreamReader(getAssets().open(fileName), StandardCharsets.UTF_8))) {
      String[] vals;
      while ((vals = reader.readNext()) != null) {
        streetAndPos.put(vals[0],new double[]{Double.parseDouble(vals[1]),Double.parseDouble(vals[2])});
      }
    } catch (Exception e) {e.printStackTrace();}
  }

  public void onResume(){
    super.onResume();
    mapView.onResume();
  }

  public void onPause(){
    super.onPause();
    mapView.onPause();
  }

  @Override
  public void onBackPressed() {
    DrawerLayout drawer = findViewById(R.id.drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }
  }

  public void openRoutInpAct(String activeInput){
    CENTER = (GeoPoint) mapView.getMapCenter();
    Intent intent = new Intent(this, RoutingInputActivity.class);
    intent.putExtra("start", startLoc);
    intent.putExtra("end", endLoc);
    intent.putExtra("activeInput", activeInput);
    startActivityForResult(intent, 1234);
    mapView.getOverlayManager().remove(rLine);
    mapView.getOverlayManager().remove(startMarker);
    mapView.getOverlayManager().remove(endMarker);
  }

  @SuppressLint("ClickableViewAccessibility")
  public void showPopup(View view, String mode, Object obj) throws Exception {
    LayoutInflater inflater = (LayoutInflater)
            getSystemService(LAYOUT_INFLATER_SERVICE);
    View popupView=null;
    PopupWindow popupWindow=null;
    Button cancelBtn=null;
    switch (mode) {
      case "ads":
        popupView = inflater.inflate(R.layout.activity_popup_ads, null);
        // create the popup window
        popupWindow = new PopupWindow(popupView, 950, 1400, true);
        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        TextView name1 = popupWindow.getContentView().findViewById(R.id.nameText);
        name1.setText("");

        WebView webView = popupWindow.getContentView().findViewById(R.id.webViewPopup);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadUrl("");
        cancelBtn = popupWindow.getContentView().findViewById(R.id.adsPopCancelBut);
        break;
      case "cam":
        Camera cam = (Camera) obj;
        popupView = inflater.inflate(R.layout.activity_popup_camera, null);
        popupWindow = new PopupWindow(popupView, getResources().getDisplayMetrics().widthPixels,
                getResources().getDisplayMetrics().heightPixels, true);
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        TextView street = popupWindow.getContentView().findViewById(R.id.streetTextView);
        street.setText(cam.getStreetName());
        TextView status = popupWindow.getContentView().findViewById(R.id.statusTextView);
        status.setText(cam.getStatus()+"%");
        TextView date = popupWindow.getContentView().findViewById(R.id.dateTextView);
        date.setText(cam.getLast_update());
        ImageView camImg = popupWindow.getContentView().findViewById(R.id.cameraImg);
        try {
          InputStream iStream = (InputStream) cam.getImgUrl().getContent();
          Drawable img = Drawable.createFromStream(iStream, "src");
          camImg.setImageDrawable(img);
        }catch (Exception e){}
        cancelBtn = popupWindow.getContentView().findViewById(R.id.camPopCancelBut);
        break;
      case "help":
        popupView = inflater.inflate(R.layout.activity_popup_help, null);
        popupWindow = new PopupWindow(popupView, 1200, 2300, true);
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
        cancelBtn = popupWindow.getContentView().findViewById(R.id.helpPopCancelBut);
    }
    final PopupWindow finalPopupWindow = popupWindow;
    cancelBtn.setOnClickListener(view1 -> finalPopupWindow.dismiss());

    // dismiss the popup window when touched
//    popupView.setOnTouchListener((v, event) -> {
//      finalPopupWindow.dismiss();
//      return true;
//    });
  }

//public void showPopup(View view, String mode, Object obj) throws Exception{
//  LayoutInflater inflater = (LayoutInflater)
//          getSystemService(LAYOUT_INFLATER_SERVICE);
//  View popupView=null;
//  PopupWindow popupWindow=null;
//  Button cancelBtn;
//  switch (mode) {
//    case "ads":
//      popupView = inflater.inflate(R.layout.activity_popup_ads, null);
//      // create the popup window
//      popupWindow = new PopupWindow(popupView, 950, 1400, true);
//      // show the popup window
//      // which view you pass in doesn't matter, it is only used for the window tolken
//      popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
//
//      TextView name1 = popupWindow.getContentView().findViewById(R.id.nameText);
//      name1.setText("");
//
//      WebView webView = popupWindow.getContentView().findViewById(R.id.webViewPopup);
//      WebSettings webSettings = webView.getSettings();
//      webSettings.setJavaScriptEnabled(true);
//      webView.loadUrl("");
//      cancelBtn = popupWindow.getContentView().findViewById(R.id.adsPopCancelBut);
//      break;
//    case "cam":
//      Camera cam = (Camera) obj;
//      popupView = inflater.inflate(R.layout.activity_popup_camera, null);
//      popupWindow = new PopupWindow(popupView, 950, 1400, true);
//      popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
//
//      TextView street = popupWindow.getContentView().findViewById(R.id.streetTextView);
//      street.setText(cam.getStreetName());
//      TextView status = popupWindow.getContentView().findViewById(R.id.statusTextView);
//      status.setText(cam.getStatus());
//      TextView date = popupWindow.getContentView().findViewById(R.id.dateTextView);
//      date.setText(cam.getLast_update());
//      ImageView camImg = popupWindow.getContentView().findViewById(R.id.cameraImg);
//      InputStream iStream = (InputStream) cam.getImgUrl().getContent();
//      Drawable img = Drawable.createFromStream(iStream, "src");
//      camImg.setImageDrawable(img);
//
//      cancelBtn = popupWindow.getContentView().findViewById(R.id.camPopCancelBut);
//      PopupWindow finalPopupWindow1 = popupWindow;
//      cancelBtn.setOnClickListener(view1 -> finalPopupWindow1.dismiss());
//  }
//
//  // dismiss the popup window when touched
//  PopupWindow finalPopupWindow = popupWindow;
//  popupView.setOnTouchListener((v, event) -> {
//    finalPopupWindow.dismiss();
//    return true;
//  });
//}

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data){
    switch (requestCode){
      case 1234:
        if(resultCode == RESULT_OK && data != null){
          if(data.getExtras().getInt("mode")==1){
            routingInfoLayout.setVisibility(View.VISIBLE);
            choosePointLayout.setVisibility(View.INVISIBLE);

            startLoc = (MapPoint) data.getSerializableExtra("start");
            endLoc = (MapPoint) data.getSerializableExtra("end");

            if(startLoc.getMode()==1) {
              Location myLoc = locProvider.getLastKnownLocation();
              startLoc.setLocation(myLoc.getLatitude(),myLoc.getLongitude());
            }

            startTextView.setText(startLoc.getLabel());
            endTextView.setText(endLoc.getLabel());
            try{
              new Router(this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,startLoc.getLat(),startLoc.getLon(),endLoc.getLat(),endLoc.getLon());
            }catch (Exception ex) {
              ex.printStackTrace();
            }
          }else{
            activeInput = data.getExtras().getString("activeInput");
            routingInfoLayout.setVisibility(View.INVISIBLE);
            choosePointLayout.setVisibility(View.VISIBLE);
          }
        }
    }
  }

  //-----------ROUTING-----------
  @Override
  public void drawRoute(JSONArray pArr, double distance) throws Exception{
    this.rArray = pArr;
    if (rLine!=null) mapView.getOverlayManager().remove(this.rLine);
    //Add user pos
    if(startLoc.getMode()==1) {
      Location loc = locProvider.getLastKnownLocation();
      pArr.put(0, new JSONArray().put(loc.getLatitude()).put(loc.getLongitude()));
      startNavBut.setVisibility(View.VISIBLE);
    }else {
      startNavBut.setVisibility(View.INVISIBLE);
    }
    rLine = addRoute(pArr, false);

    double minLat = Double.MAX_VALUE;
    double maxLat = Double.MIN_VALUE;
    double minLon = Double.MAX_VALUE;
    double maxLon = Double.MIN_VALUE;

    for (GeoPoint p : this.rLine.getPoints()) {
        minLat = (p.getLatitude() < minLat) ? p.getLatitude():minLat;
        maxLat = (p.getLatitude() > maxLat) ? p.getLatitude():maxLat;
        minLon = (p.getLongitude() < minLon) ? p.getLongitude():minLon;
        maxLon = (p.getLongitude() > maxLon) ? p.getLongitude():maxLon;
    }
    BoundingBox boundingBox = new BoundingBox(maxLat, maxLon, minLat, minLon);
    mapView.zoomToBoundingBox(boundingBox.increaseByScale(1.7f),true);
    //Start and end marker
    if(startLoc.getMode()!=1){
      JSONArray startPos = pArr.getJSONArray(0);
      this.startMarker = addMarker(startPos.getDouble(0), startPos.getDouble(1), R.drawable.ic_start_marker);
    }
    JSONArray endPos = pArr.getJSONArray(pArr.length()-1);
    this.endMarker = addMarker(endPos.getDouble(0),endPos.getDouble(1),R.drawable.ic_end_marker);
    //Show distance
    distanceTextView.setVisibility(View.VISIBLE);
    distanceTextView.setText(distance+" km");
  }

  private void removeRoutingLine() {
    this.mapView.getOverlayManager().remove(this.rLine);
    this.mapView.getOverlayManager().remove(this.startMarker);
    this.mapView.getOverlayManager().remove(this.endMarker);
  }

  @Override
  public Polyline addRoute(JSONArray pArr, boolean isSub) throws Exception{
    Polyline line;
    line = new Polyline(mapView);
    line.setWidth(13f);
    line.setColor(getResources().getColor((isSub) ? R.color.colorSubRouting:R.color.colorRouting));
    List<GeoPoint> pts = new ArrayList<>();
    JSONArray j;
    for(int i=0;i<pArr.length();i++) {
      j = pArr.getJSONArray(i);
      pts.add(new GeoPoint(j.getDouble(0),j.getDouble(1)));
    }

    line.setPoints(pts);
    line.setGeodesic(true);
    line.getPaint().setStrokeCap(Paint.Cap.ROUND);
    line.setInfoWindow(new BasicInfoWindow(R.layout.bonuspack_bubble, mapView));
    mapView.getOverlayManager().add(line);
    return line;
  }
  //-----------/ROUTING-----------

  //-----------Navigation mode-----------
  private void startNavigation() {
    this.mapView.getOverlays().remove(searchMarker);
    this.mapView.getOverlays().remove(myCurrentPos);
    this.mapView.getOverlays().add(myLocationNav);
    Location loc = locProvider.getLastKnownLocation();
    mapView.getController().animateTo(new GeoPoint(loc.getLatitude(),loc.getLongitude()),(double) NAV_ZOOM,2000L);
    followUser = true;
  }

  private void endNavigation() {
    this.mapView.getOverlays().remove(myLocationNav);
    mapView.getOverlayManager().remove(rLine);
    mapView.getOverlayManager().remove(startMarker);
    mapView.getOverlayManager().remove(endMarker);
    mapView.getOverlayManager().add(myCurrentPos);
    rLine=null;
    mapView.getController().animateTo(CENTER,(double) DEFAULT_ZOOM,700L);
    mapView.setMapOrientation(0);
    followUser = false;
  }

  //-----------/Navigation mode-----------

  public Marker addMarker(double lat, double lon, int src){
    Marker marker = new Marker(mapView);
    marker.setPosition(new GeoPoint(lat,lon));
    marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
    marker.setIcon(getResources().getDrawable(src,null));
    mapView.getOverlays().add(marker);
    return marker;
  }

  @Override
  public void onLocationChanged(Location location, IMyLocationProvider source) {
    if (!followUser)
      return;
//      updateMyCurrentPos(new GeoPoint(location.getLatitude(),location.getLongitude()),myCompass.getOrientation());
//    float t = (360 - myCompass.getOrientation());
//    if (t < 0) t += 360;
//    if (t > 360) t -= 360;
//    t = (int) t;
//    t = t / 5;
//    t = (int) t;
//    t = t * 5;
    try {
      JSONArray n = this.rArray.getJSONArray(1);
      JSONArray n1 = this.rArray.getJSONArray(2);
      float t = (float) angleFromCoordinate(location.getLatitude(),location.getLongitude(),n.getDouble(0),n.getDouble(1));

      float t1 = (float) angleFromCoordinate(n.getDouble(0),n.getDouble(1),n1.getDouble(0),n1.getDouble(1));

      this.mapView.setMapOrientation(360-t);
      this.mapView.getController().animateTo(new GeoPoint(location.getLatitude(),location.getLongitude()));

      updateDirectionText(t-t1);

      Log.d("GGGGGGGGGG",t+"X"+t1);
    } catch (JSONException e) {
      e.printStackTrace();
    }
  }

  private void updateDirectionText(float angle) {
    int nextText=-1,nextIcon=-1;
    Log.d("GGGGGGGGGG",""+angle);
    if(angle>0) {
      nextText = R.string.turn_left;
      nextIcon = R.drawable.ic_turn_left;
    } else if(angle<0) {
      nextText = R.string.turn_right;
      nextIcon = R.drawable.ic_turn_right;
    } else {
      nextText = R.string.go_straight_ahead;
      nextIcon = R.drawable.ic_go_straight;
    }
    nextDirTextView.setText(nextText);
    nextDirIcon.setImageResource(nextIcon);
  }

  private double angleFromCoordinate(double lat1, double lon1, double lat2, double lon2) {
    double dLon = (lon2 - lon1);
    double y = Math.sin(dLon) * Math.cos(lat2);
    double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
            * Math.cos(lat2) * Math.cos(dLon);

    double brng = Math.atan2(y, x);

    brng = Math.toDegrees(brng);
    brng = (brng + 360) % 360;
    brng = 360 - brng; // count degrees counter-clockwise - remove to make clockwise

    return brng;
  }

//  //Update current user position
//  private void updateMyCurrentPos(GeoPoint pos, float angle) {
//    Bitmap target = RotateMyBitmap(userStandBitmap, angle);
//    mapView.getOverlays().remove(myCurrentPos);
//    myCurrentPos = new Marker(mapView);
//    myCurrentPos.setPosition(pos);
//    myCurrentPos.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
//    myCurrentPos.setIcon(new BitmapDrawable(getResources(), target));
//    mapView.getOverlays().add(myCurrentPos);
//  }

  public static Bitmap RotateMyBitmap(Bitmap source, float angle) {
    Matrix matrix = new Matrix();
    matrix.postRotate(angle);
//    return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, false);
    return source;
  }

  private void requestPermissions() {
    ActivityCompat.requestPermissions(MainActivity.this,
            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.ACCESS_FINE_LOCATION},
            1);
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    for(int i : grantResults)
      if(i==-1)
        finishAndRemoveTask();
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }
}