package com.example.smrt;

import android.app.Activity;
import android.content.Intent;

import com.example.smrt.utils.PopUp;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.view.Gravity;
import android.view.MenuItem;

public class NavBarListener implements NavigationView.OnNavigationItemSelectedListener {
    private Activity act;

    public NavBarListener(Activity ctx) {
        this.act = ctx;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.nav_feedback:
                intent = new Intent(act, FeedbackActivity.class);
                act.startActivity(intent);
                break;
            case R.id.nav_setting:
                intent = new Intent(act, SettingActivity.class);
                act.startActivity(intent);
                break;
            case R.id.nav_help:
                try {
                    ((PopUp) this.act).showPopup((act).getWindow().getDecorView().getRootView(),"help","");
                } catch (Exception e) {}
        }
        DrawerLayout drawer = act.findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
