package com.example.smrt;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.smrt.model.MapPoint;

public class RoutingInputActivity extends AppCompatActivity {
    ImageView backIcon, startIcon;
    MapPoint startLoc, endLoc;
    AutoCompleteTextView startTextView, endTextView;
    TextView selectOnMap,goBtn;
    String activeInput = null;
    ConstraintLayout myPosBtn;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routing_input);

        backIcon = findViewById(R.id.backIcon);
        selectOnMap = findViewById(R.id.selectOnMapTextView);
        startTextView = findViewById(R.id.startTextView2);
        endTextView = findViewById(R.id.endTextView2);
        myPosBtn = findViewById(R.id.myPosBtn);
        goBtn = findViewById(R.id.goBtn);
        startIcon = findViewById(R.id.startIcon);

        Intent intent = getIntent();
        startLoc = (MapPoint) intent.getSerializableExtra("start");
        endLoc = (MapPoint) intent.getSerializableExtra("end");

        startIcon.setImageResource((startLoc.getMode()==1) ? R.drawable.ic_user_pos:R.drawable.ic_start);

        startTextView.setText(startLoc.getLabel());
        endTextView.setText(endLoc.getLabel());

        activeInput = intent.getStringExtra("activeInput");
        if(activeInput.equals("start"))
            startTextView.requestFocus();
        else
            endTextView.requestFocus();
        startTextView.setOnTouchListener(
                (view, motionEvent) -> {
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) activeInput="start";
                    myPosBtn.setVisibility(View.VISIBLE);
                    return false;
                }
        );
        endTextView.setOnTouchListener(
                (view, motionEvent) -> {
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) activeInput="end";
                    myPosBtn.setVisibility(View.GONE);
                    return false;
                }
        );
        backIcon.setOnClickListener(view -> back2Main(1));
        goBtn.setOnClickListener(view -> back2Main(1));
        selectOnMap.setOnClickListener(view -> back2Main(2));

        startTextView.setAdapter(MainActivity.adapterStreetName);
        startTextView.setOnItemClickListener((adapterView, view, i, l) -> {
            startLoc.setMode(2);
            startLoc.setLabel(adapterView.getItemAtPosition(i).toString());
            startLoc.updateMode2();
            startTextView.setText(adapterView.getItemAtPosition(i).toString());
            startIcon.setImageResource(R.drawable.ic_start);
            removeFocus(startTextView);
        });
        endTextView.setAdapter(MainActivity.adapterStreetName);
        endTextView.setOnItemClickListener((adapterView, view, i, l) -> {
            endLoc.setMode(2);
            endLoc.setLabel(adapterView.getItemAtPosition(i).toString());
            endLoc.updateMode2();
            endTextView.setText(adapterView.getItemAtPosition(i).toString());
            removeFocus(endTextView);
        });
        myPosBtn.setOnClickListener(view -> {
            startLoc.setMode(1);
            startTextView.setText(getString(R.string.your_position));
            startIcon.setImageResource(R.drawable.ic_user_pos);
        });
    }

    private void removeFocus(View v) {
        v.clearFocus();
        ((InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
    }


    private void back2Main(int mode) {
        Intent i = new Intent(RoutingInputActivity.this, MainActivity.class);
        if(mode==1){
            i.putExtra("mode",1);
        } else if(mode==2){ //Mode for choose point
            if(activeInput.equals("start")) startLoc.setMode(3);
            else endLoc.setMode(3);
            i.putExtra("mode",2);
            i.putExtra("activeInput",activeInput);
        }
        startLoc.setLabel(startTextView.getText().toString());
        endLoc.setLabel(endTextView.getText().toString());
        i.putExtra("start", startLoc);
        i.putExtra("end", endLoc);
        setResult(RESULT_OK, i);
        finish();
    }
}
