package com.example.smrt;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.smrt.api.CamsLoader;

public class SettingActivity extends AppCompatActivity {
    EditText setEditText;
    ImageView setBackBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        setEditText = findViewById(R.id.ipEditText);
        setEditText.setText(CamsLoader.IP_ADDRESS);

        setBackBtn = findViewById(R.id.setBackBtn);
        setBackBtn.setOnClickListener(view -> {
            CamsLoader.updateIP(setEditText.getText().toString());
            finish();});
    }
}
