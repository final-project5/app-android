package com.example.smrt.api;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.smrt.MainActivity;
import com.example.smrt.R;
import com.example.smrt.maputils.MapListener4Camera;
import com.example.smrt.model.Camera;
import com.example.smrt.utils.PopUp;

import org.json.JSONArray;
import org.json.JSONObject;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class CamsLoader extends AsyncTask<Void, Void, Void> implements MapListener4Camera.MapListenerCamera{
    //10.0.3.2
    public static final String IP_KEY = "IP_ADDRESS";
    public static String IP_ADDRESS = "http://10.0.3.2:9999";
    private static final String CAMS_LOCATION = "/api/cameras";
    private static String CAMS_SOURCE = IP_ADDRESS + CAMS_LOCATION;
    private static final int RELOAD_TIME = 7000;
    private static boolean firstLoad = true;
    private final MapListener4Camera mapListener;

    public static void updateIP(String newIP) {
        IP_ADDRESS = newIP;
        CAMS_SOURCE = IP_ADDRESS + CAMS_LOCATION;
        Router.ROUTE_SOURCE = IP_ADDRESS + Router.ROUTE_LOCATION;
        MainActivity.prefs.edit().putString(IP_KEY,IP_ADDRESS).commit();
    }

    private Context ctx;
    private MapView mapView;
    private HashMap<String,Camera> camList;

    public CamsLoader(Context context, MapView mapView) {
        this.ctx = context;
        this.mapView = mapView;
        this.camList = new HashMap<>();
        //Show camera at specific zoom
        mapListener = new MapListener4Camera(this);
        this.mapView.setMapListener(mapListener);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            while (true) {
                String data = loadData();
                updateData(data);
                for (Camera cam : camList.values()) {
                    cam.setMarker(updateMarker(cam));
                }
                Thread.sleep(RELOAD_TIME);
            }
        }catch (Exception e) {e.printStackTrace();}
        return null;
    }

    private Marker updateMarker(Camera cam) {
        if(cam.getMarker()!=null)
            mapView.getOverlayManager().remove(cam.getMarker());
        Marker marker = new Marker(mapView);
        marker.setPosition(cam.getPosition());
        int iconCode = (cam.getStatus()>70) ? ((cam.canGo()) ? R.drawable.ic_cctv_red:R.drawable.ic_cctv_black):(cam.getStatus()>30) ? R.drawable.ic_cctv_yellow:R.drawable.ic_cctv_green;
        marker.setIcon(ctx.getResources().getDrawable(iconCode,null));
        String id = cam.getId();
        marker.setId(id);
        marker.setOnMarkerClickListener((m, mapView) -> {
            try {
                Log.d("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX","XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                ((PopUp) this.ctx).showPopup(((Activity)ctx).getWindow().getDecorView().getRootView(),"cam",camList.get(m.getId()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        });
        if(mapListener.isShowing())
            mapView.getOverlays().add(marker);
        return marker;
    }

    private String loadData() {
        String result = null;
        HttpURLConnection connection = null;
        BufferedReader reader = null;

        try {
            URL url = new URL(CAMS_SOURCE);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            InputStream stream = connection.getInputStream();

            reader = new BufferedReader(new InputStreamReader(stream));
            result = reader.readLine();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(connection!=null) connection.disconnect();
            if(reader!=null) {
                try {
                    reader.close();
                } catch (IOException e) {e.printStackTrace();}
            }
        }
        return result;
    }

    private void updateData(String s) {
        try {
            JSONArray jObj = new JSONArray(s);
            String id;
            if (!firstLoad) {
                for(int i=0;i<jObj.length();i++){
                    JSONObject j = jObj.getJSONObject(i);
                    Camera cam = camList.get(j.getString("id"));
                    cam.setStatus(j.getInt("status"));
                    cam.setImgUrl(j.getString("last_image"));
                    cam.setLast_update(j.getString("last_update"));
                    cam.setCango(j.getString("can_go"));
                }
            }else {
                firstLoad = false;
                for(int i=0;i<jObj.length();i++) {
                    JSONObject j = jObj.getJSONObject(i);
                    id = j.getString("id");
                    Camera camera = new Camera(id, j.getInt("status"), j.getString("streetname"), j.getString("last_update"), new GeoPoint(j.getDouble("lon"), j.getDouble("lat")), IP_ADDRESS + j.getString("last_image"),j.getString("can_go"));
                    this.camList.put(id, camera);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showCam() {
        for(Camera cam:camList.values()) {
            mapView.getOverlayManager().add(cam.getMarker());
        }
    }

    @Override
    public void hideCam() {
        for(Camera cam:camList.values()) {
            mapView.getOverlayManager().remove(cam.getMarker());
        }
    }
}
