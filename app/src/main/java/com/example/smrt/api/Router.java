package com.example.smrt.api;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.smrt.utils.Routing;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Router extends AsyncTask<Double,String,String> {


    public static final String ROUTE_LOCATION = "/api/routing/";
    public static String ROUTE_SOURCE = CamsLoader.IP_ADDRESS +ROUTE_LOCATION;

    private Context ctx;

    public Router(Context context) {
        this.ctx = context;
    }
    @Override
    protected String doInBackground(Double... poses) {
        String result = null;
        HttpURLConnection connection = null;
        BufferedReader reader = null;
        try {
            URL url = new URL(ROUTE_SOURCE +poses[0]+","+poses[1]+"/"+poses[2]+","+poses[3]);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            InputStream stream = connection.getInputStream();

            reader = new BufferedReader(new InputStreamReader(stream));
            result = reader.readLine();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(connection!=null) connection.disconnect();
            if(reader!=null) {
                try {
                    reader.close();
                } catch (IOException e) {e.printStackTrace();}
            }
        }
        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        try {
            JSONObject jObj = new JSONObject(s);
            JSONArray list = jObj.getJSONArray("list");
            ((Routing) ctx).drawRoute(list,jObj.getDouble("distance"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
