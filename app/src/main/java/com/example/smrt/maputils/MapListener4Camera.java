package com.example.smrt.maputils;

import com.example.smrt.MainActivity;
import com.example.smrt.api.CamsLoader;

import org.osmdroid.events.MapListener;
import org.osmdroid.events.ScrollEvent;
import org.osmdroid.events.ZoomEvent;



//Support showing camera follow range
public class MapListener4Camera implements MapListener {

    private MapListenerCamera ctx;
    private boolean isShowing = false;

    public MapListener4Camera(CamsLoader camsLoader) {
        this.ctx = camsLoader;
    }

    @Override
    public boolean onScroll(ScrollEvent event) {
        return false;
    }

    @Override
    public boolean onZoom(ZoomEvent event) {
        if(event.getZoomLevel()>= 14) {
            if(isShowing) return true;
            ctx.showCam();
            isShowing=true;
        }else if (isShowing) {
            ctx.hideCam();
            isShowing=false;
        }
        return true;
    }

    public interface MapListenerCamera {
        void showCam();
        void hideCam();
    }

    public boolean isShowing() {
        return isShowing;
    }
}
