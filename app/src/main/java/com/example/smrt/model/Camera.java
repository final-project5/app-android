package com.example.smrt.model;


import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.Marker;
import java.io.IOException;
import java.net.URL;

public class Camera {
    private String id,streetName,last_update;
    private GeoPoint position;
    private URL imgUrl;
    private int status;
    private boolean canGo;
    private Marker marker;

    public Camera(String id, int weight, String streetName, String last_update, GeoPoint position, String imgURL, String canGo) {
        this.id = id;
        this.status = weight;
        this.streetName = streetName;
        this.last_update = last_update;
        this.position = position;
        this.canGo = canGo.equals("yes") ? true:false;
        try {
            this.imgUrl = new URL(imgURL);
        } catch (IOException e) {}
    }

    public String getId() {
        return id;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getLast_update() {
        return last_update;
    }

    public GeoPoint getPosition() {
        return position;
    }

    public int getStatus() {
        return status;
    }

    public URL getImgUrl() {
        return imgUrl;
    }

    public Marker getMarker() {
        return marker;
    }

    public boolean canGo() {return  canGo; }

    public void setMarker(Marker marker) {
        this.marker=marker;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public void setImgUrl(String imgUrl) {
        try {
            this.imgUrl = new URL(imgUrl);
        } catch (IOException e) {}
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setCango(String canGo) {
        this.canGo = canGo.equals("yes") ? true:false;
    }
}
