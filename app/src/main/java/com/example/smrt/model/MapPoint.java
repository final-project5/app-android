package com.example.smrt.model;

import com.example.smrt.MainActivity;

import java.io.Serializable;

public class MapPoint implements Serializable {
    private String label;
    private int mode;
    private double lat,lon;

    //1 is user, 2 is location name, 3 is lat lon
    public MapPoint(int mode) {
        this.label = (mode==1) ? "Your position":"";
        this.mode = mode;
    }

    public String getLabel() {
        return label;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public int getMode() {
        return mode;
    }

    public void setLocation(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setMode(int mode) {
        this.mode = mode;
        this.label = (mode==1) ? "Your position":this.label;
    }

    public void updateMode2() {
        this.lat = MainActivity.streetAndPos.get(this.label)[0];
        this.lon = MainActivity.streetAndPos.get(this.label)[1];
    }
}
