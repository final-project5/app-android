package com.example.smrt.utils;

import android.view.View;

public interface PopUp {
    void showPopup(View view, String mode, Object obj) throws Exception;
}
