package com.example.smrt.utils;

import org.json.JSONArray;
import org.osmdroid.views.overlay.Polyline;

public interface Routing {
    void drawRoute(JSONArray pArr, double distance) throws Exception;
    Polyline addRoute(JSONArray pArr, boolean isSub) throws Exception;
}
